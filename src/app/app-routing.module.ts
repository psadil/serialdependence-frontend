import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TrialComponent } from './trial/trial.component';
import { InstructComponent } from './instruct/instruct.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { FullscreenComponent } from './fullscreen/fullscreen.component';
import { EndComponent } from './end/end.component';

const appRoutes: Routes = [
  { path: '', redirectTo: 'welcome', pathMatch: 'full' },
  { path: "welcome", component: FullscreenComponent },
  { path: 'instruct/:id', component: InstructComponent },
  { path: "experiment", component: TrialComponent },
  { path: "end", component: EndComponent },
  { path: '**', component: PageNotFoundComponent },
]

@NgModule({
  imports: [ RouterModule.forRoot( appRoutes ) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule{}
