export interface Participant {
  id: string;
  userAgent: string;
}
