import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';

import { v4 as uuidv4 } from 'uuid';

import { Trial } from './trial/trial.interface';
import { Participant } from './participant.interface';


@Injectable({
  providedIn: 'root',
})
export class InMemoryDataService implements InMemoryDbService {

  createDb() {
    const Participant = {id: "99", userAgent: "hello"};
    const Trial = {id: 99,
      participant: "99",
      trial: 0,
      orientation: 0,
      response: 0,
      time_response: 0,
      time_grating_on: 0,
      time_grating_off: 0,
      time_render_off: 0,};
    return {Participant, Trial};
  }

  isTrial(model: Trial | Participant): model is Trial {
    return (model as Trial).participant !== undefined;
  }

  // Overrides the genId method to ensure that ids are always unique
  // note that there is already a trial field, so it ought to be
  // easy to recover the order of trials
  genId<T extends Trial | Participant>(_model: T[]): string {
    return uuidv4();
  }

}
