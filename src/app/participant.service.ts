import { Injectable } from '@angular/core';

import { v4 as uuidv4 } from 'uuid';

import { Participant } from './participant.interface';


@Injectable({
  providedIn: 'root'
})
export class ParticipantService {

  participant: Participant = {
    id: uuidv4(),
    userAgent: window.navigator.userAgent
  }

  constructor() { }

}
