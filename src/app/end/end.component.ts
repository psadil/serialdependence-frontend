import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-end',
  templateUrl: './end.component.html',
  styleUrls: ['./end.component.css']
})
export class EndComponent implements OnInit {

  message: string = `You have reached the end of the experiment! Thanks for participanting.`;

  constructor() { }

  ngOnInit(): void {
    document.exitFullscreen()
  }

}
