import { Injectable } from '@angular/core';

import * as PIXI from "pixi.js"

@Injectable({
  providedIn: 'root'
})
export class PixiappService {

  // this will be our pixi application
  app: any;
  ticker: any;

  constructor() {
    this.app = new PIXI.Application({
      transparent: true,
      autoStart: false,
      sharedTicker: true,
      resizeTo: window
    });
    this.ticker = PIXI.Ticker.shared;
    this.ticker.autoStart = false;
  }
}
