import { TestBed } from '@angular/core/testing';

import { PixiappService } from './pixiapp.service';

describe('PixiappService', () => {
  let service: PixiappService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PixiappService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
