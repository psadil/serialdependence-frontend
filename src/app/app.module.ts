import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './in-memory-data.service';

import { AppComponent } from './app.component';
import { TrialComponent } from './trial/trial.component';
import { AppRoutingModule } from './app-routing.module';
import { InstructComponent } from './instruct/instruct.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { FullscreenComponent } from './fullscreen/fullscreen.component';
import { EndComponent } from './end/end.component';

/**
 *
 * TODO
 *   - implement blocks
 *   - connect design to django backend
 *   - record expt version (gitsha) in participants model
 *   - consent form
 *   - make warnings/cancel happen if participant exits fullscreen
 *   - test everything
 *   - - testing stubs?
 *   - - double-check timing
 *   - - double-check sizing/position
 *   - boostrap? https://ng-bootstrap.github.io/#/components/buttons/examples
 **/


@NgModule({
  declarations: [
    AppComponent,
    TrialComponent,
    InstructComponent,
    PageNotFoundComponent,
    FullscreenComponent,
    EndComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,

    // The HttpClientInMemoryWebApiModule module intercepts HTTP requests
    // and returns simulated server responses.
    // Remove it when a real server is ready to receive requests.
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, {
        dataEncapsulation: false,
        passThruUnknownUrl: true }
      ),

    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
