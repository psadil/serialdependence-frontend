import { TestBed } from '@angular/core/testing';

import { PointerlockService } from './pointerlock.service';

describe('PointerlockService', () => {
  let service: PointerlockService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PointerlockService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
