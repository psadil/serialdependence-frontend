import { Injectable } from '@angular/core';

import * as PIXI from "pixi.js"

import { PixiappService } from '../pixiapp.service';

@Injectable({
  providedIn: 'root'
})
export class FixationService {

  point: any = new PIXI.Graphics()
  .beginFill(0xFFFFFF, 1)
  .drawCircle(this.pixiapp.app.screen.width/2, this.pixiapp.app.screen.height/2, 2)
  .endFill();

  constructor(private pixiapp: PixiappService) { }

}
