import { Component, OnInit, ViewChild } from '@angular/core';

import { fromEvent } from 'rxjs';

import afterFrame from "afterframe";

import { PixiappService } from '../pixiapp.service';
import { GratingService } from './grating.service';
import { ResponsedotsService } from './responsedots.service';
import { DesignService } from '../design.service';
import { ParticipantService } from '../participant.service';
import { Trial } from './trial.interface';
import { ActivatedRoute, Router } from '@angular/router';
import { FixationService } from './fixation.service';
import { PointerlockService } from './pointerlock.service';

@Component({
  selector: 'app-trial',
  templateUrl: './trial.component.html',
  styleUrls: ['./trial.component.css']
})
export class TrialComponent implements OnInit {
  // this allows us to reference and load stuff into the div container
  @ViewChild('pixiContainer', { static: true })
  private _pixiContainer: any;

  trial: number = 0;
  response: number | undefined;
  time_response: number | undefined;
  iti: number = 500;
  bar_visible: boolean = false;
  grating_duration: number = 0;
  time_grating_on: number | undefined;
  time_grating_off: number | undefined;

  // fake pointer coordinates
  resp_coords: number[] = [this.pixiapp.app.screen.width/2, this.pixiapp.app.screen.height/2];

  // hacky: where to go next?
  private _next: number | undefined;
  private _demo: boolean = false;
  // will navigate to /end when this.trial == (_trials - 1)
  private _trials: number = 10;

  // Create an Observable that will publish mouse movements
  private _mouseMoves: any;
  private _subscription: any;

  private static _RAD_TO_DEG: number = 180 / Math.PI;

  // timer needs to account for frame rate of monitor (fixed to 60, above)
  // by subtracting 0.5 * (1/fps), the grating should be removed in time
  private static _slack: number = 1000/120;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private designService: DesignService,
    private pixiapp: PixiappService,
    private grating: GratingService,
    private responsedots: ResponsedotsService,
    private pointer: PointerlockService,
    private fixation: FixationService,
    private participantService: ParticipantService
  ) { }

  ngOnInit(): void {

    this.route.queryParamMap.subscribe(params => {
      this._next = Number(params.get('next'));
      this._demo = params.get('demo') === 'true';
      this.grating_duration = Number(params.get('grating_duration')) || 200;
      this.bar_visible = params.get('bar_visible') === 'true';
    });

    this.grating.orientation = Math.floor(Math.random() * 179);

    // place pixi application onto the viewable document
    this._pixiContainer.nativeElement.appendChild(this.pixiapp.app.view);

    // lock (and hide) pointer to pixi app
    this._pixiContainer.nativeElement.requestPointerLock();
    this.pointer.pointer.position.set(this.pixiapp.app.screen.width/2, this.pixiapp.app.screen.height/2);

    this._mouseMoves = fromEvent(this._pixiContainer.nativeElement, 'mousemove');
    this._subscription = this._mouseMoves.subscribe((evt: MouseEvent) => {this.updatePosition(evt)});

    this.pixiapp.ticker.add(() => {
      this.responsedots.set_resp(this.get_ori_from_mouse(this.resp_coords));
    });

    this.grating.container.on('added', () => { this.start_timing() });
    this.responsedots.resp_circle0.on('removed', () => { this.do_iti() });
    this.grating.container.on('removed', () => { this.log_grating_off() });

    // https://pixijs.download/dev/docs/PIXI.BasePrepare.html
    // Load object into GPU. when done loading, start the render
    this.pixiapp.app.renderer.plugins.prepare.upload(
      this.grating.container, () => {
        this.pixiapp.app.stage.addChild(
          this.grating.container,
          this.responsedots.resp_circle0,
          this.responsedots.resp_circle1,
          this.fixation.point,
          this.responsedots.bar,
          this.pointer.pointer);

          this.responsedots.bar.visible = this.bar_visible;

        this.pixiapp.app.start();
        }
    );
  }

  ngOnDestroy(): void {
    document.exitPointerLock();
    this._subscription.unsubscribe();
    this.responsedots.resp_circle0.off('removed');
    this.grating.container.off('removed');
  }

  updatePosition(evt: MouseEvent): void {
    this.resp_coords[0] += evt.movementX;
    this.resp_coords[1] += evt.movementY;

    if (this.resp_coords[0] > this._pixiContainer.nativeElement.width + this.pointer.radius) {
      this.resp_coords[0] = -this.pointer.radius;
    }
    if (this.resp_coords[1] > this._pixiContainer.nativeElement.height + this.pointer.radius) {
      this.resp_coords[1] = -this.pointer.radius;
    }
    if (this.resp_coords[0] < -this.pointer.radius) {
      this.resp_coords[0] = this._pixiContainer.nativeElement.width + this.pointer.radius;
    }
    if (this.resp_coords[1] < -this.pointer.radius) {
      this.resp_coords[1] = this._pixiContainer.nativeElement.height + this.pointer.radius;
    }

    // this part actually updates pointer position
    this.pointer.pointer.position.set(this.resp_coords[0], this.resp_coords[1]);
  }

  get_ori_from_mouse(coords: number[]): number {
    return TrialComponent._RAD_TO_DEG * Math.atan2(
      this.pixiapp.app.screen.height/2 - coords[1],
      coords[0] - this.pixiapp.app.screen.width/2);
  }

  log_click(event: any): void {
    this.pixiapp.app.stage.removeChildren();
    this.response = this.get_ori_from_mouse([this.resp_coords[0], this.resp_coords[1]]);
    this.time_response = event.timeStamp
  }

  add(trial: Trial): void {
    this.designService.add_trial(trial)
      .subscribe();
  }

  start_timing(): void {

    // https://github.com/andrewiggins/afterframe
    // https://nolanlawson.com/2018/09/25/accurately-measuring-layout-on-the-web/
    // still not quite right! but this seems like the best option, for now
    // based on stopping during debug, the grating has at least been drawn
    // when the callbak in afterFrame is executed
    // requestPostAnimationFrame will be what I need when it's avaialble...
    afterFrame((timestamp) => {
      this.time_grating_on = timestamp;
      if (isFinite(this.grating_duration)){
        setTimeout(() => { this.pixiapp.app.stage.removeChild(this.grating.container) },
        this.grating_duration - TrialComponent._slack);
      };
    });
  }

  log_grating_off(): void {
    afterFrame((timestamp) => {
      this.time_grating_off = timestamp;
      if (isFinite(this.grating_duration)){
        setTimeout(() => { this.pixiapp.app.stage.removeChild(this.grating.container) },
        this.grating_duration - TrialComponent._slack);
      };
    });
  }

  do_iti(): void {

    if(this._demo){
      this.router.navigate(['/instruct/'+(this._next)]);
    } else {
      afterFrame((timestamp) => {
        // reset pointer to center of screen
        this.resp_coords[0] = this.pixiapp.app.screen.width/2;
        this.resp_coords[1] = this.pixiapp.app.screen.height/2;
        this.pointer.pointer.position.set(this.pixiapp.app.screen.width/2, this.pixiapp.app.screen.height/2);

        // send data to server
        this.add({
          participant: this.participantService.participant.id,
          orientation: this.grating.orientation,
          trial: this.trial,
          response: this.response,
          time_response: this.time_response,
          time_grating_on: this.time_grating_on,
          time_grating_off: this.time_grating_off,
          time_render_off: timestamp
        } as Trial);

        if (this.trial === this._trials) {
          this.router.navigate(['/end']);
        } else {
          setTimeout(() => {
            this.trial++;
            this.grating.orientation = Math.floor(Math.random() * 179);

            this.pixiapp.app.stage.addChild(
              this.grating.container,
              this.responsedots.resp_circle0,
              this.responsedots.resp_circle1,
              this.fixation.point,
              this.responsedots.bar,
              this.pointer.pointer);
          },
          this.iti - TrialComponent._slack);
        }
      });
    };
  }
}
