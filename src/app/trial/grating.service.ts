import { Injectable } from '@angular/core';

import * as PIXI from "pixi.js"

import { PixiappService } from '../pixiapp.service';

@Injectable({
  providedIn: 'root'
})
export class GratingService {

    _orientation: number = 0;
    radius: number = 128;
    spatial_frequency: number = 0.02;

    container: any = new PIXI.Container();

    private _freq_two_pi: number;
    private _coef: number[];
    private _grating: any = new PIXI.Container();

    private static _frag: string =
    `
    precision highp float;
    uniform vec2 coef;
    const vec3 basecolor = vec3(1.0, 1.0, 1.0) * 0.5;

    void main() {

        /* Evaluate sine grating at requested position, frequency and phase: */
        float sv = sin(dot(coef, gl_FragCoord.xy));

        /* Multiply/Modulate base color and alpha with calculated sine            */
        /* values, add some constant color/alpha Offset, assign as final fragment */
        /* output color: */
        gl_FragColor = vec4((basecolor * sv) + 0.5, 1.0);
    }
    `;

    constructor(private pixiapp: PixiappService) {
      this._freq_two_pi = this.spatial_frequency * Math.PI * 2;
      this._coef = this.set_coef(this._orientation);

      let filter = new PIXI.Filter(undefined, GratingService._frag, {coef: this._coef});

      let circle = new PIXI.Graphics()
     .beginFill()
     .drawCircle(this.pixiapp.app.screen.width/2, this.pixiapp.app.screen.height/2, this.radius)
     .endFill();

      this._grating.filterArea = new PIXI.Rectangle(
      this.pixiapp.app.screen.width/4,
      this.pixiapp.app.screen.height/4,
      this.pixiapp.app.screen.width/2,
      this.pixiapp.app.screen.height/2);

      this.container.addChild(this._grating);
      this._grating.filters = [filter];
      this.container.mask = circle;
    }

    set orientation(ori: number){
      this._orientation = ori;
      this._coef = this.set_coef(ori);
      let filter = new PIXI.Filter(undefined, GratingService._frag, {coef: this._coef});
      this._grating.filters = [filter];
    }

    get orientation(): number{
      return(this._orientation);
    }

    set_coef(orientation: number) {
      let rotation = orientation * PIXI.DEG_TO_RAD;
      return [Math.cos(rotation) * this._freq_two_pi, Math.sin(rotation) * this._freq_two_pi];
    }

}
