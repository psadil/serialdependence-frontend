import { Injectable } from '@angular/core';

import * as PIXI from "pixi.js";

import { PixiappService } from '../pixiapp.service';

@Injectable({
  providedIn: 'root'
})
export class ResponsedotsService {

  orientation: number = 0;

  radius: number = 256;
  resp_circle0: any = new PIXI.Graphics()
      .beginFill(0xFFFFFF, 1)
      .drawCircle(0, 0, 10)
      .endFill();
  resp_circle1: any = new PIXI.Graphics()
      .beginFill(0xFFFFFF, 1)
      .drawCircle(0, 0, 10)
      .endFill();

  bar: any = new PIXI.Graphics()
      .beginFill(0xFFFFFF, 1)
      .drawRect(0, 0, this.radius*2, 1)
      .endFill();

  constructor(private pixiapp: PixiappService) { }

  set_resp(ori: number): void {
    const rad0 = PIXI.DEG_TO_RAD * ori;
    const ori0 = [this.pixiapp.app.screen.width/2 + Math.cos(rad0)*this.radius, this.pixiapp.app.screen.height/2 - Math.sin(rad0)*this.radius];
    const rad1 = PIXI.DEG_TO_RAD * (ori + 180);
    const ori1 = [this.pixiapp.app.screen.width/2 + Math.cos(rad1)*this.radius, this.pixiapp.app.screen.height/2 - Math.sin(rad1)*this.radius];

    this.resp_circle0.position.set(ori0[0], ori0[1]);
    this.resp_circle1.position.set(ori1[0], ori1[1]);
    this.bar.position.set(this.resp_circle1.position.x, this.resp_circle1.position.y);
    this.bar.angle = -ori;
  }

}
