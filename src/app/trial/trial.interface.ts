export interface Trial {
  id: number;
  participant: string;
  trial: number;
  orientation: number;
  response: number;
  time_response: number;
  time_grating_on: number;
  time_grating_off: number;
  time_render_off: number;
}
