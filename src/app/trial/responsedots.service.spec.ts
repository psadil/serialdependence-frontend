import { TestBed } from '@angular/core/testing';

import { ResponsedotsService } from './responsedots.service';

describe('ResponsedotsService', () => {
  let service: ResponsedotsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ResponsedotsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
