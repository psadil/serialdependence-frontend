import { Injectable } from '@angular/core';

import * as PIXI from "pixi.js";

import { PixiappService } from '../pixiapp.service';

@Injectable({
  providedIn: 'root'
})
export class PointerlockService {

  radius: number = 5;
  pointer: any = new PIXI.Graphics()
      .beginFill(0xff0000, 1)
      .drawCircle(0, 0, this.radius)
      .endFill();

  constructor(private pixiapp: PixiappService) { }
}
