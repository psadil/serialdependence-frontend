import { TestBed } from '@angular/core/testing';

import { FixationService } from './fixation.service';

describe('FixationService', () => {
  let service: FixationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FixationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
