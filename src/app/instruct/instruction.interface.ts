export interface Instruction {
  id: number;
  message: string[];
}
