import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

import { Instruction } from './instruction.interface'

@Component({
  selector: 'app-instruct',
  templateUrl: './instruct.component.html',
  styleUrls: ['./instruct.component.css']
})
export class InstructComponent implements OnInit {

  instructions: Instruction[] = [
    {id: 0, message: ["Welcome to the experiment!",
      `You will see circular patch with black and white stripes, surrounded by two white dots. Your task will
       be to use the cursor to move the two dots until they line up with the stripes`,
      "Click next to see an example"]},
    {id: 1, message: [`During the experiment, the two dots will not be connected with a line. You will need to imagine
      a line connecting them, and your task will be to align this imagined line with the stripes`,
      "Click next for an example"]},
    {id: 2, message: [`Finally, the black and white stripes will only be visible for a moment. So pay close attention!`,
    "Click next to see an example."]},
    {id: 3, message: [`The experiment will begin when you click next.`]}
      ];

  message: string[] | undefined;

  private id: number = 0;

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.id = Number(params.get('id'));
      this.message = this.instructions[this.id].message;
    });
  }

  next(): void {
    switch (this.id) {
      case 0:
        this.router.navigate(['/experiment'], { queryParams: {
          next: 1,
          demo: true,
          grating_duration: 'Infinity',
          bar_visible: true} });
        break;
      case 1:
        this.router.navigate(['/experiment'], { queryParams: {
          next: 2,
          demo: true,
          grating_duration: 'Infinity',
          bar_visible: false} });
          break
      case 2:
        this.router.navigate(['/experiment'], { queryParams: {
          next: 3,
          demo: true,
          bar_visible: false} });
        break;
      default:
        this.router.navigate(['/experiment']);
    }
  }

  back(): void {
    this.location.back();
  }

}
