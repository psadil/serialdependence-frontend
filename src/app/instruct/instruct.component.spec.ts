import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from "@angular/router/testing";

import { InstructComponent } from './instruct.component';

describe('InstructComponent', () => {
  let component: InstructComponent;
  let fixture: ComponentFixture<InstructComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstructComponent ],
      imports: [ RouterTestingModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstructComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
