import { Component, OnInit, Inject } from '@angular/core';
// import { DOCUMENT } from '@angular/common';
import { Router } from '@angular/router';
import { DesignService } from '../design.service';
import { ParticipantService } from '../participant.service';

// https://developer.mozilla.org/en-US/docs/Web/API/Element/requestFullscreen
// import { HTMLElement } from './document.interface'

/*

TODO: https://blog.bradleygore.com/2015/01/04/html5-fullscreen-api-angular-directives/

*/

@Component({
  selector: 'app-fullscreen',
  templateUrl: './fullscreen.component.html',
  styleUrls: ['./fullscreen.component.css']
})
export class FullscreenComponent implements OnInit {

  constructor(
    private router: Router,
    private designService: DesignService,
    private participantService: ParticipantService
  ) { }

  ngOnInit(): void {
    this.designService.add_participant(this.participantService.participant)
    .subscribe();
  }

  start(): void {
    let element  = document.documentElement;
    element.requestFullscreen()
      .then(() => this.router.navigate(['/instruct/0']));
  }

}
