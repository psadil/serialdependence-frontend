export const environment = {
  production: true,
  _trialURL: 'serialdependence/trial',
  _participantURL: 'serialdependence/participant'
};
